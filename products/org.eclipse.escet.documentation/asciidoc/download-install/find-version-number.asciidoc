/////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2020 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available under the terms
// of the MIT License which is available at https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
/////////////////////////////////////////////////////////////////////////////////

include::_part_attributes.asciidoc[]

indexterm:[version, find]

[[download-install-find-version]]
== Finding the tool's version number

From the Eclipse ESCET IDE, there are multiple ways to find out which version
of the toolkit or specific tools you have currently installed:

* Via the Eclipse _About Eclipse ESCET_ dialog.
+
The _About Eclipse ESCET_ dialog can be opened via the
menu:Help[About Eclipse ESCET] menu of the Eclipse ESCET IDE. The dialog has
shows the version of the Eclipse ESCET toolkit. An btn:[Installation Details]
button is available to open the _Eclipse Installation Details_ dialog. In this
dialog, the _Installed Software_ tab shows all the installed software,
including their versions under the _Version_ column.

* Via the option dialogs of the various tools.
+
Most of the Eclipse ESCET tools can be started in a way that shows the option
dialog for that tool. All option dialogs for our tools have a _Help_ category.
By clicking on that category, the help text for that tool is shown.
The help text includes the version of the tool that you are using.

* Via the command line option, in a ToolDef script.
+
If you start an application using a ToolDef script, you can specify command
line arguments in the script as well. Start a tool with the `-h` or `--help`
option to see the command line help text, which includes the version.

For command line scripts, the following approach is recommended:

* Start a tool with the `-h` or `--help` option to see the command line help
text, which includes the version.

/////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2020 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available under the terms
// of the MIT License which is available at https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
/////////////////////////////////////////////////////////////////////////////////

// Including common attributes to set for all documents within this directory.
include::_root_attributes.asciidoc[]

// Enable custom style in index-docinfo.html.
:docinfo: private

// Use this for asciidoc documents containing a title.
:doctype: book


= Chi documentation (Incubation)
:author: Copyright (c) 2010, 2020 Contributors to the Eclipse Foundation
:title-logo-image: {imgsdir}/eclipse-incubation.png
:favicon: favicon.png

Chi is a modeling language for describing and analyzing performance of
discrete event systems by means of simulation.
It uses a process-based view, and uses synchronous point-to-point
communication between processes. A process is written as an imperative
program, with a syntax much inspired by the well-known Python language.

Chi is one of the tools of the
link:https://eclipse.org/escet[Eclipse ESCET(TM) project].

[WARNING]
====
The Eclipse ESCET project, including the Chi language and toolset,
is currently in the
link:https://wiki.eclipse.org/Development_Resources/Process_Guidelines/What_is_Incubation[Incubation Phase].

image:{imgsdir}/eclipse-incubation.png[,width=300,pdfwidth=45%]
====

ifdef::website-output[]
TIP: You can link:eclipse-escet-incubation-chi-manual.pdf[download this manual]
as a PDF as well.
endif::website-output[]

Tutorial::
The <<tut-chapter-chi-tutorial>>
teaches the Chi language, and its use in modeling and simulating systems
to answer your performance questions.
+
Some interesting topics are:
+
* Basics (<<tut-chapter-data-types>>, <<tut-chapter-statements>>,
<<tut-chapter-stochastic-behavior>>)
* Programming (<<tut-chapter-processes>>, <<tut-chapter-channels>>)
* Modeling (<<tut-chapter-buffers>>, <<tut-chapter-servers-with-time>>,
<<tut-chapter-conveyors>>)

Reference manual::
The <<ref-chapter-reference-manual>>
describes the Chi language in full detail, for example the top level language
elements or all statements. It also contains a list with all standard library
functions and a list with all distribution functions.
+
Some interesting topics are:
+
* <<ref-chapter-global-definitions>> (Top level language elements)
* <<ref-chapter-standard-library>> (Standard library functions)
* <<ref-chapter-distributions>> (Available distributions)

Tool manual::
The <<tool-chapter-tool-manual,Tool manual>>
describes the Chi simulator software.
Use of the software to create and simulate Chi programs is also explained.

Release notes::
The <<release-notes-chapter-index,Release notes>> provides information on
all Chi releases.

Legal::
See <<legal-chapter-index,Legal>> for copyright and licensing information.


// Tutorial
include::tutorial/tutorial.asciidoc[]

:leveloffset: +1

include::tutorial/introduction.asciidoc[]

// Tutorial - Basics
include::tutorial/datatypes.asciidoc[]

include::tutorial/statements.asciidoc[]

include::tutorial/functions.asciidoc[]

include::tutorial/input-output.asciidoc[]

// Tutorial - Programming
include::tutorial/stochastic-behavior.asciidoc[]

include::tutorial/processes.asciidoc[]

include::tutorial/channels.asciidoc[]

// Tutorial - Modeling
include::tutorial/buffers.asciidoc[]

include::tutorial/servers.asciidoc[]

include::tutorial/conveyors.asciidoc[]

include::tutorial/experiments.asciidoc[]

// Tutorial - Visualization
include::tutorial/svg-vis.asciidoc[]

include::tutorial/svg-example.asciidoc[]

:leveloffset: -1

// Reference manual
include::reference-manual/reference-manual.asciidoc[]

:leveloffset: +1

include::reference-manual/global.asciidoc[]

include::reference-manual/statements.asciidoc[]

include::reference-manual/expressions.asciidoc[]

include::reference-manual/stdlib.asciidoc[]

include::reference-manual/distributions.asciidoc[]

include::reference-manual/types.asciidoc[]

include::reference-manual/lexicals.asciidoc[]

include::reference-manual/migration.asciidoc[]

include::reference-manual/svg-ref.asciidoc[]

:leveloffset: -1

// Tool manual
include::tool-manual/tool-manual.asciidoc[]

:leveloffset: +1

include::tool-manual/operation.asciidoc[]

include::tool-manual/commandline.asciidoc[]

:leveloffset: -1

// Release notes
include::release-notes.asciidoc[]

// Legal
include::legal.asciidoc[]


ifdef::pdf-output[]
[index]
== Index
endif::pdf-output[]

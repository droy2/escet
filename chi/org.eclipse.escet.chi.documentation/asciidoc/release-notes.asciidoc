/////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2020 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available under the terms
// of the MIT License which is available at https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
/////////////////////////////////////////////////////////////////////////////////

include::_root_attributes.asciidoc[]

indexterm:[release, notes]

[[release-notes-chapter-index]]
== Chi release notes

The release notes for the releases of Chi and the associated tools, as
part of the Eclipse ESCET project, are listed below in reverse chronological
order.

=== Version 0.1 (unreleased)

The first release of Chi as part of the Eclipse ESCET project.

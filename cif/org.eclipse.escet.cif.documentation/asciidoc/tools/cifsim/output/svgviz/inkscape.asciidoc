/////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2020 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available under the terms
// of the MIT License which is available at https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
/////////////////////////////////////////////////////////////////////////////////

include::../../../_part_attributes.asciidoc[]

[[tools-cifsim-output-svgviz-chapter-inkscape]]
== Inkscape

indexterm:[SVG,Inkscape]
indexterm:[Inkscape]
SVG images can best be edited with a drawing program that supports vector
graphics. One such program is
link:http://inkscape.org/[Inkscape],
a free and
open source vector graphics editor that uses SVG as its native file format, and
is available for many platforms, including Microsoft Windows, Mac OS X, and
Linux. In this documentation, we'll use Inkscape to create and edit SVG images.

It is beyond the scope of this document to explain how Inkscape works. For
that, see the
link:http://inkscape.org/[Inkscape website].
Here is a screenshot of the Inkscape interface:

image::{tools-imgsdir}/cifsim/output/svgviz/inkscape/inkscape.png[]

indexterm:[Inkscape,changing ids]

=== Changing ids

As described on the <<tools-cifsim-output-svgviz-chapter-svg,page about SVG>>,
every SVG object has an id. To change the id of an object in Inkscape, right
click on the object, and choose
menu:Object Properties[].
The
menu:Object Properties[]
window will appear, which looks like this:

image::{tools-imgsdir}/cifsim/output/svgviz/inkscape/inkscape_props_xml.png[]

In this window, you can change the object's id, by entering a new id in the
menu:Id[]
field, and clicking the
btn:[Set]
button.

indexterm:[Inkscape,XML editor]

=== XML editor

Inkscape also features an XML editor, that can be used to view and edit the
underlying XML representation of the SVG image. The screenshot above also shows
Inkscape's
menu:XML Editor[]
window, which can be opened via the
menu:XML Editor...[]
item in the
menu:Edit[]
menu. The XML editor
can be useful to find out the exact structure of the XML tree, and to see the
precise definitions that were used to represent the objects that you created
on Inkscape's canvas.

indexterm:[Inkscape,document size]
indexterm:[Inkscape,width]
indexterm:[Inkscape,height]

[[tools-cifsim-output-svgviz-inkscape-size]]
=== Document size

It is recommended to always set the correct size of the image, before adding
any shapes or text labels. To do this, select
menu:Document Properties...[]
from the
menu:File[]
menu, to open the
menu:Document Properties[]
window. On the
menu:Page[]
tab, we
recommend to set the
menu:Units[]
to
menu:px[].
You can then enter
the
menu:Width[]
and
menu:Height[]
of the image.

By default, Inkscape always adds a layer to new images. When changing the size
of the image as described above, this layer gets a `transform` attribute
that can cause all kinds of trouble later on. There are several ways to get
rid of this `transform` attribute of the layer:

* Select
menu:Layers...[]
from the
menu:Layer[]
menu to show the
menu:Layers[]
panel. In that panel, select
menu:Layer 1[]
by
clicking on it. Then click the
menu:Delete the current layer[]
button
to remove the selected layer.

* Use the
menu:XML Editor[]
to select the layer
(`<svg:g id="layer1"`... node). Then click the
menu:Delete node[]
button at the top of the
menu:XML Editor[]
window to remove the layer.
Alternatively, after selecting the layer, press the kbd:[Delete] key on the
keyboard to delete the selected layer.

* Use the
menu:XML Editor[]
to select the layer
(`<svg:g id="layer1"`... node). Then, on the right, select the
`transform` attribute by clicking on it. Then click the
menu:Delete attribute[]
button at the top of the
menu:XML Editor[]
window to remove the attribute.

The first two approaches remove the layer, the third approach keeps the layer
and only removes the `transform` attribute.

Alternatively, there is also a way to avoid the `transform` attribute from
being added in the first place. Instead of using the
menu:Document Properties[]
window to change the size of the image, do
the following:

* Use the
menu:XML Editor[]
to select the entire image
(`<svg:svg`... node).

* Select the `width` attribute on the right, by clicking on it.

* Change the value in the text area at the lower right corner of the window.

* Click the
menu:Set[]
button to save the new width value. Alternatively,
after typing the new value, press kbd:[Ctrl+ENTER] to save the
new width value.

* Repeat for the `height` attribute.

indexterm:[Inkscape,coordinate system]

=== Coordinate systems

The coordinate system used by Inkscape uses the lower left
corner of the canvas as origin, while the underlying SVG representation uses
the upper left corner of the canvas as origin. Also, Inkscape uses x and y
coordinates of objects relative to the outside of their border (called _stroke_
in Inkscape), while SVG uses x and y coordinates of objects relative to the
middle of their border. These are two most commonly encountered differences
between Inkscape and SVG. See the <<tools-cifsim-output-svgviz-chapter-example-tank,tank example>>
for some tips and tricks regarding the difference in coordinate systems.

indexterm:[Inkscape,text area]
indexterm:[Inkscape,flowRoot]
indexterm:[SVG,flowRoot]

[[tools-cifsim-output-svgviz-inkscape-textarea]]
=== Text areas

When using the Inkscape text tool, select the tool, and left click once on the
canvas to add a text label. Once you added it, start typing text.

Inkscape also allows you to select the text tool, left click anywhere on the
canvas, drag the mouse pointer to another position, and only then release the
left mouse button. This creates a text area, where text is automatically
wrapped to multiple lines, so that it stays within the bounds of the text area.
This feature uses SVG `flowRoot` elements. These elements are defined in
version 1.2 of the SVG standard, which is currently still a working draft.
These elements are _not_ supported by the CIF simulator, which only supports
version 1.1 of the SVG standard.

Using an SVG image with a `flowRoot` or other unsupported SVG feature,
results in the following error when the SVG file is
<<tools-cifsim-output-svgviz-chapter-viewer,opened>> in Eclipse:


[source, console]
----
ERROR: SVG image file "some_image.svg" is not an SVG file, is an invalid SVG file, or contains unsupported SVG features.
CAUSE: invalid.element
----

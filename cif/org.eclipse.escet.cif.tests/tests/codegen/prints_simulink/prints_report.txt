Input/output report of the prints.cif SFunction.

During code generation, CIF variables are made available in the Simulink vectors.
This report lists the variables in each vector, along with their index number.

Modes
-----
a1 1

Continuous states
-----------------
time 1

Inputs
------
No variables are available here.

Outputs
-------
a1 1

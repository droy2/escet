Input/output report of the types_and_literals.cif SFunction.

During code generation, CIF variables are made available in the Simulink vectors.
This report lists the variables in each vector, along with their index number.

Modes
-----
a 1

Continuous states
-----------------
time 1

Inputs
------
No variables are available here.

Outputs
-------
a 1

State 1:
    Initial: true
    Marked: false

    Locations:
        location "X" of automaton "A"

    Edges:
        edge A.e goto state 1

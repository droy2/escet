//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2020 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

event nn, ns, na; // First letter is A usage (never, sometimes, always)
event sn, ss, sa; // Second letter is B usage (never, sometimes, always)
event an, as, aa;

automaton A:
  location:
    initial;
    edge nn, ns, na;
    edge sn, ss, sa;          // Sometimes reads B.x
    edge sn, ss, sa when B.x;
    edge an, as, aa when B.x; // Always reads B.x
end

automaton B:
  disc bool x;
  location:
    initial;
    edge nn, sn, an;
    edge ns, ss, as;          // Sometimes reads B.x
    edge ns, ss, as when B.x;
    edge na, sa, aa when B.x; // Always reads B.x
end

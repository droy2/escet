//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2020 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.parser.ast;

import java.util.List;

import org.eclipse.escet.cif.parser.ast.tokens.AIdentifier;
import org.eclipse.escet.cif.parser.ast.tokens.AName;
import org.eclipse.escet.common.position.metamodel.position.Position;

/** Formal component parameter. */
public class AFormalComponentParameter extends AFormalParameter {
    /** The component definition type of the parameter. */
    public final AName type;

    /** The names of the parameters. */
    public final List<AIdentifier> names;

    /**
     * Constructor for the {@link AFormalComponentParameter} class.
     *
     * @param type The component definition type of the parameter.
     * @param names The names of the parameters.
     * @param position Position information.
     */
    public AFormalComponentParameter(AName type, List<AIdentifier> names, Position position) {
        super(position);
        this.type = type;
        this.names = names;
    }
}
